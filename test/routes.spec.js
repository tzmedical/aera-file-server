/* eslint-disable func-names */
/* eslint-disable max-nested-callbacks */
/* eslint-disable no-sync */
/* eslint-env node, mocha */
/*eslint max-nested-callbacks: ["error", 4]*/

'use strict'
const config = require('../app/config')
const app = require('../app')
const { encrypt } = require('../app/encryption')
const chai = require('chai')
const fs = require('fs')
const chaiHttp = require('chai-http')
const sinon = require('sinon')
const { decrypt } = require('./helper')

const expect = chai.expect

const util = require('../app/util')
const stubSandbox = sinon.sandbox.create()

chai.config.includeStack = true
chai.use(chaiHttp)

describe('Route testing', function () {
  this.timeout(10 * 1000)

  const testingDeviceSerial = 'H3R4002026'
  const testFilesBasePath = './test/files/decrypted-files'

  const testFileTZA = fs.readFileSync(`${testFilesBasePath}/test.tza`)
  const testFileTZE = fs.readFileSync(`${testFilesBasePath}/test.tze`)
  const testFileSCP = fs.readFileSync(`${testFilesBasePath}/test.scp`)
  const testFileTZR = fs.readFileSync(`${testFilesBasePath}/test.tzr`)
  const testFileTZS = fs.readFileSync(`${testFilesBasePath}/test.tzs`)

  const binaryParser = (res, cb) => {
    res.setEncoding('binary')
    res.data = ''
    res.on('data', (chunk) => {
      res.data += chunk
    })
    res.on('end', () => {
      cb(null, Buffer.from(res.data, 'binary'))
    })
  }

  describe('Serial begins with Number', () => {
    // TODO just make sure that the action that is returned is empty
  })

  describe('Serial begins with Letter', () => {
    // Tested with both GCS disabled/enabled
    if (config.features.storage) {
      before(() => {
        return (
          util
            // Seed test file tza files
            .writeFile(`${config.env}/files/test.tza`, testFileTZA)
            // Seed test file tzs files
            .then(() => util.writeFile(`${config.env}/files/test.tzs`, testFileTZS))
            // Seed test file tze files
            .then(() => util.writeFile(`${config.env}/files/test.tze`, testFileTZE))
            // Seed test file tzr files
            .then(() => util.writeFile(`${config.env}/files/test.tzr`, testFileTZR))
            // Seed test file scp files
            .then(() => util.writeFile(`${config.env}/files/test.scp`, testFileSCP))

            // Seed test file tza for specific test device requests
            .then(() =>
              util.writeFile(
                `${config.env}/files/actions/${testingDeviceSerial}/${testingDeviceSerial}.tza`,
                testFileTZA
              )
            )

            // Seed test file tzs for specific test device requests
            .then(() =>
              util.writeFile(
                `${config.env}/files/settings/${testingDeviceSerial}/${testingDeviceSerial}.tzs`,
                testFileTZS
              )
            )
        )
      })

      describe('Google Cloud Storage Enabled', () => {
        if (config.encryptionEnabled) {
          describe('Encryption Enabled', () => {
            it('should accept GET /actions and respond with the file', (done) => {
              const tzaFileName = 'test.tza'
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              let actionExpectedToBeSent

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  actionExpectedToBeSent = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/actions/download/${testingDeviceSerial}`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                      }

                      expect(encrypt(actionExpectedToBeSent, config.key, config.iv)).to.deep.equal(
                        res.body
                      )
                      done()
                    })
                })
            })

            it('should accept GET /settings and respond with the file', (done) => {
              const tzsFileName = 'test.tzs'
              const tzsFileLocation = `${config.env}/files/${tzsFileName}`
              let settingsExpectedToBeSent

              util
                .readFile(tzsFileLocation)
                .then((data) => {
                  settingsExpectedToBeSent = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/settings/download/H3R4002026`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      expect(
                        encrypt(settingsExpectedToBeSent, config.key, config.iv)
                      ).to.deep.equal(res.body)
                      done()
                    })
                })
            })

            it('should accept GET / and 200 OK', (done) => {
              chai
                .request(app)
                .get(`/`)
                .end((err, res) => {
                  if (err) {
                    done(err)
                  }

                  expect(res).to.have.status(200)
                  done()
                })
            })

            it('should accept POST /events and store the file', (done) => {
              const tzeFileName = 'test.tze'
              const tzaFileName = 'test.tza'
              let tzeFileContents, tzaFileContents

              const tzeFileLocation = `${config.env}/files/${tzeFileName}`
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              const proccessedTZEFileLocation = `${config.env}/files/events/${testingDeviceSerial}/${tzeFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => util.readFile(tzeFileLocation))
                .then((data) => {
                  tzeFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/events/upload/${testingDeviceSerial}/${tzeFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzeFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZEFileLocation)
                        .then((processedTZEFileContents) => {
                          expect(processedTZEFileContents).to.deep.equal(
                            decrypt(tzeFileContents, config.key, config.iv)
                          )
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /ecgs and store the file', (done) => {
              const scpFileName = 'test.scp'
              const tzaFileName = 'test.tza'
              let scpFileContents, tzaFileContents

              const scpFileLocation = `${config.env}/files/${scpFileName}`
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              const proccessedSCPFileLocation = `${config.env}/files/ecgs/${testingDeviceSerial}/${scpFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => util.readFile(scpFileLocation))
                .then((data) => {
                  scpFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/ecgs/upload/${testingDeviceSerial}/${scpFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(scpFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedSCPFileLocation)
                        .then((processedTZEFileContents) => {
                          expect(processedTZEFileContents).to.deep.equal(
                            decrypt(scpFileContents, config.key, config.iv)
                          )
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /reports and store the file', (done) => {
              const tzrFileName = 'test.tzr'
              const tzaFileName = 'test.tza'
              let tzrFileContents, tzaFileContents

              const tzrFileLocation = `${config.env}/files/${tzrFileName}`
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              const proccessedTZRFileLocation = `${config.env}/files/reports/${testingDeviceSerial}/${tzrFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => util.readFile(tzrFileLocation))
                .then((data) => {
                  tzrFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/reports/upload/${testingDeviceSerial}/${tzrFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzrFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZRFileLocation)
                        .then((processedTZRFileContents) => {
                          expect(processedTZRFileContents).to.deep.equal(
                            decrypt(tzrFileContents, config.key, config.iv)
                          )
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /events, store the file, and return no actions file', (done) => {
              const tzeFileName = 'test.tze'
              let tzeFileContents

              const tzeFileLocation = `${config.env}/files/${tzeFileName}`
              const proccessedTZEFileLocation = `${config.env}/files/events/H3R4012026/${tzeFileName}`

              util
                .readFile(tzeFileLocation)
                .then((data) => {
                  tzeFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/events/upload/H3R4012026/${tzeFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzeFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZEFileLocation)
                        .then((processedTZEFileContents) => {
                          expect(processedTZEFileContents).to.deep.equal(
                            decrypt(tzeFileContents, config.key, config.iv)
                          )
                          expect(res.body).to.deep.equal(Buffer.from(''))
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })
          })
        } else {
          describe('Encryption Disabled', () => {
            it('should accept GET /actions and respond with the file', (done) => {
              const tzaFileName = 'test.tza'
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              let actionExpectedToBeSent

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  actionExpectedToBeSent = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/actions/download/${testingDeviceSerial}`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                      }

                      expect(actionExpectedToBeSent).to.deep.equal(res.body)
                      done()
                    })
                })
            })

            it('should accept GET /settings and respond with the file', (done) => {
              const tzsFileName = 'test.tzs'
              const tzsFileLocation = `${config.env}/files/${tzsFileName}`
              let settingsExpectedToBeSent

              util
                .readFile(tzsFileLocation)
                .then((data) => {
                  settingsExpectedToBeSent = data

                  if (!config.features.storage) {
                    stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                      callback(null, settingsExpectedToBeSent)
                    })
                  }
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/settings/download/H3R4002026`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      expect(settingsExpectedToBeSent).to.deep.equal(res.body)
                      done()
                    })
                })
            })

            it('should accept POST /events and store the file', (done) => {
              const tzeFileName = 'test.tze'
              const tzaFileName = 'test.tza'
              let tzeFileContents, tzaFileContents

              const tzeFileLocation = `${config.env}/files/${tzeFileName}`
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              const proccessedTZEFileLocation = `${config.env}/files/events/${testingDeviceSerial}/${tzeFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data
                })
                .then(() => util.readFile(tzeFileLocation))
                .then((data) => {
                  tzeFileContents = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/events/upload/${testingDeviceSerial}/${tzeFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzeFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZEFileLocation)
                        .then((processedTZEFileContents) => {
                          expect(processedTZEFileContents).to.deep.equal(tzeFileContents)
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /ecgs and store the file', (done) => {
              const scpFileName = 'test.scp'
              const tzaFileName = 'test.tza'
              let scpFileContents, tzaFileContents

              const scpFileLocation = `${config.env}/files/${scpFileName}`
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              const proccessedSCPFileLocation = `${config.env}/files/ecgs/${testingDeviceSerial}/${scpFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data
                })
                .then(() => util.readFile(scpFileLocation))
                .then((data) => {
                  scpFileContents = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/ecgs/upload/${testingDeviceSerial}/${scpFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(scpFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedSCPFileLocation)
                        .then((processedSCPFileContents) => {
                          expect(processedSCPFileContents).to.deep.equal(scpFileContents)
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /reports and store the file', (done) => {
              const tzrFileName = 'test.tzr'
              const tzaFileName = 'test.tza'
              let tzrFileContents, tzaFileContents

              const tzrFileLocation = `${config.env}/files/${tzrFileName}`
              const tzaFileLocation = `${config.env}/files/${tzaFileName}`
              const proccessedTZRFileLocation = `${config.env}/files/reports/${testingDeviceSerial}/${tzrFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data
                })
                .then(() => util.readFile(tzrFileLocation))
                .then((data) => {
                  tzrFileContents = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/reports/upload/${testingDeviceSerial}/${tzrFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzrFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZRFileLocation)
                        .then((processedTZRFileContents) => {
                          expect(processedTZRFileContents).to.deep.equal(tzrFileContents)
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept GET / and 200 OK', (done) => {
              chai
                .request(app)
                .get(`/`)
                .end((err, res) => {
                  if (err) {
                    done(err)
                  }

                  expect(res).to.have.status(200)
                  done()
                })
            })
          })
        }
      })
    }
    // Tested with both GCS disabled/enabled
    else {
      describe('Google Cloud Storage Disabled', () => {
        if (config.encryptionEnabled) {
          describe('Encryption Enabled', () => {
            it('should accept GET /actions and respond with the file', (done) => {
              const tzaFileName = 'test.tza'
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`

              let actionExpectedToBeSent

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  actionExpectedToBeSent = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, actionExpectedToBeSent)
                  })
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/actions/download/${testingDeviceSerial}`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                      }

                      expect(encrypt(actionExpectedToBeSent, config.key, config.iv)).to.deep.equal(
                        res.body
                      )
                      done()
                    })
                })
            })

            it('should accept GET /settings and respond with the file', (done) => {
              const tzsFileName = 'test.tzs'
              const tzsFileLocation = `test/files/decrypted-files/${tzsFileName}`
              let settingsExpectedToBeSent

              util
                .readFile(tzsFileLocation)
                .then((data) => {
                  settingsExpectedToBeSent = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, settingsExpectedToBeSent)
                  })
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/settings/download/H3R4002026`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      expect(
                        encrypt(settingsExpectedToBeSent, config.key, config.iv)
                      ).to.deep.equal(res.body)
                      done()
                    })
                })
            })

            it('should accept GET / and 200 OK', (done) => {
              chai
                .request(app)
                .get(`/`)
                .end((err, res) => {
                  if (err) {
                    done(err)
                  }

                  expect(res).to.have.status(200)
                  done()
                })
            })

            it('should accept POST /events and store the file', (done) => {
              const tzeFileName = 'test.tze'
              const tzaFileName = 'test.tza'
              let tzeFileContents, tzaFileContents

              const tzeFileLocation = `test/files/decrypted-files/${tzeFileName}`
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`
              const proccessedTZEFileLocation = `files/events/${testingDeviceSerial}/${tzeFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, tzaFileContents)
                  })
                })
                .then(() => util.readFile(tzeFileLocation))
                .then((data) => {
                  tzeFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/events/upload/${testingDeviceSerial}/${tzeFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzeFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZEFileLocation)
                        .then((processedTZEFileContents) => {
                          // Verify the TZE file
                          expect(
                            encrypt(processedTZEFileContents, config.key, config.iv)
                          ).to.deep.equal(tzeFileContents)
                          // Verify the TZA response file
                          expect(res.body).to.deep.equal(
                            encrypt(tzaFileContents, config.key, config.iv)
                          )
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /ecgs and store the file', (done) => {
              const scpFileName = 'test.scp'
              const tzaFileName = 'test.tza'
              let scpFileContents, tzaFileContents

              const scpFileLocation = `test/files/decrypted-files/${scpFileName}`
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`
              const proccessedSCPFileLocation = `files/ecgs/${testingDeviceSerial}/${scpFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, tzaFileContents)
                  })
                })
                .then(() => util.readFile(scpFileLocation))
                .then((data) => {
                  scpFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/ecgs/upload/${testingDeviceSerial}/${scpFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(scpFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedSCPFileLocation)
                        .then((processedTZEFileContents) => {
                          expect(
                            encrypt(processedTZEFileContents, config.key, config.iv)
                          ).to.deep.equal(scpFileContents)
                          expect(res.body, config.key, config.iv).to.deep.equal(
                            encrypt(tzaFileContents, config.key, config.iv)
                          )
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /reports and store the file', (done) => {
              const tzrFileName = 'test.tzr'
              const tzaFileName = 'test.tza'
              let tzrFileContents, tzaFileContents

              const tzrFileLocation = `test/files/decrypted-files/${tzrFileName}`
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`
              const proccessedTZRFileLocation = `files/reports/${testingDeviceSerial}/${tzrFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, tzaFileContents)
                  })
                })
                .then(() => util.readFile(tzrFileLocation))
                .then((data) => {
                  tzrFileContents = encrypt(data, config.key, config.iv)
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/reports/upload/${testingDeviceSerial}/${tzrFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzrFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZRFileLocation)
                        .then((processedTZRFileContents) => {
                          expect(
                            encrypt(processedTZRFileContents, config.key, config.iv)
                          ).to.deep.equal(tzrFileContents)
                          expect(res.body).to.deep.equal(
                            encrypt(tzaFileContents, config.key, config.iv)
                          )
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })
          })
        } else {
          describe('Encryption Disabled', () => {
            it('should accept GET /actions and respond with the file', (done) => {
              const tzaFileName = 'test.tza'
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`

              let actionExpectedToBeSent

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  actionExpectedToBeSent = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, actionExpectedToBeSent)
                  })
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/actions/download/${testingDeviceSerial}`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                      }

                      expect(actionExpectedToBeSent).to.deep.equal(res.body)
                      done()
                    })
                })
            })

            it('should accept GET /settings and respond with the file', (done) => {
              const tzsFileName = 'test.tzs'
              const tzsFileLocation = `test/files/decrypted-files/${tzsFileName}`
              let settingsExpectedToBeSent

              util
                .readFile(tzsFileLocation)
                .then((data) => {
                  settingsExpectedToBeSent = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, settingsExpectedToBeSent)
                  })
                })
                .then(() => {
                  chai
                    .request(app)
                    .get(`/settings/download/H3R4002026`)
                    .buffer()
                    .parse(binaryParser)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      expect(settingsExpectedToBeSent).to.deep.equal(res.body)
                      done()
                    })
                })
            })

            it('should accept POST /events and store the file', (done) => {
              const tzeFileName = 'test.tze'
              const tzaFileName = 'test.tza'
              let tzeFileContents, tzaFileContents

              const tzeFileLocation = `test/files/decrypted-files/${tzeFileName}`
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`
              const proccessedTZEFileLocation = `files/events/${testingDeviceSerial}/${tzeFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, tzaFileContents)
                  })
                })
                .then(() => util.readFile(tzeFileLocation))
                .then((data) => {
                  tzeFileContents = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/events/upload/${testingDeviceSerial}/${tzeFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzeFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZEFileLocation)
                        .then((processedTZEFileContents) => {
                          expect(processedTZEFileContents).to.deep.equal(tzeFileContents)
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /ecgs and store the file', (done) => {
              const scpFileName = 'test.scp'
              const tzaFileName = 'test.tza'
              let scpFileContents, tzaFileContents

              const scpFileLocation = `test/files/decrypted-files/${scpFileName}`
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`
              const proccessedSCPFileLocation = `files/ecgs/${testingDeviceSerial}/${scpFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, tzaFileContents)
                  })
                })
                .then(() => util.readFile(scpFileLocation))
                .then((data) => {
                  scpFileContents = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/ecgs/upload/${testingDeviceSerial}/${scpFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(scpFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedSCPFileLocation)
                        .then((processedTZEFileContents) => {
                          expect(processedTZEFileContents).to.deep.equal(scpFileContents)
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept POST /reports and store the file', (done) => {
              const tzrFileName = 'test.tzr'
              const tzaFileName = 'test.tza'
              let tzrFileContents, tzaFileContents

              const tzrFileLocation = `test/files/decrypted-files/${tzrFileName}`
              const tzaFileLocation = `test/files/decrypted-files/${tzaFileName}`
              const proccessedTZRFileLocation = `files/reports/${testingDeviceSerial}/${tzrFileName}`

              util
                .readFile(tzaFileLocation)
                .then((data) => {
                  tzaFileContents = data

                  stubSandbox.stub(fs, 'readFile').callsFake((path, callback) => {
                    callback(null, tzaFileContents)
                  })
                })
                .then(() => util.readFile(tzrFileLocation))
                .then((data) => {
                  tzrFileContents = data
                })
                .then(() => {
                  chai
                    .request(app)
                    .post(`/reports/upload/${testingDeviceSerial}/${tzrFileName}`)
                    .buffer()
                    .parse(binaryParser)
                    .send(tzrFileContents)
                    .end((err, res) => {
                      if (err) {
                        done(err)
                        return
                      }

                      util
                        .readFile(proccessedTZRFileLocation)
                        .then((processedTZRFileContents) => {
                          expect(processedTZRFileContents).to.deep.equal(tzrFileContents)
                          expect(res.body).to.deep.equal(tzaFileContents)
                          done()
                        })
                        .catch((error) => {
                          throw error
                        })
                    })
                })
                .catch((err) => done(err))
            })

            it('should accept GET / and 200 OK', (done) => {
              chai
                .request(app)
                .get(`/`)
                .end((err, res) => {
                  if (err) {
                    done(err)
                  }

                  expect(res).to.have.status(200)
                  done()
                })
            })
          })
        }
      })
    }
  })

  afterEach(() => {
    stubSandbox.restore()
  })
})
