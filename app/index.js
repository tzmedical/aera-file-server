/* eslint-env node */
'use strict'

// External Dependencies
const express = require('express')
const morgan = require('morgan')

// Application Dependencies
const initRoutes = require('./routes')

const app = express()

app.use(morgan('tiny'))

// Parse raw body as a Buffer
app.use(
  express.raw({
    limit: '10mb',
    type: function type() {
      return true
    },
  })
)

// Add routes from routes file
initRoutes(app)

// TODO Handle errors better for Bitrhythm (Single line of plain text)
app.use('*', (err, req, res, next) => {
  if (err) {
    const error = new Error(err.message)
    error.statusCode = 400
    return next(error)
  }
  return next()
})

// Exported for testing
module.exports = app
