/* eslint-disable no-process-env */
/* eslint-env node */
const path = require('path')

require('dotenv').config()

const config = {
  google: {},
}

config.google.projectId = process.env.GOOGLE_PROJECT

config.google.storage = {
  bucket: process.env.GOOGLE_BUCKET,
}

if (process.env.GOOGLE_AUTH) {
  try {
    config.google.auth = JSON.parse(process.env.GOOGLE_AUTH)
  } catch (err) {
    throw err
  }
}

config.encryptionEnabled = false

/**
 * Checks whether an enironment variable is set, throws if not
 *
 * @param  {string}  arg     - The value of the variable that should be set
 * @param  {string}  envName - The environment variable's name
 *
 * @throws {Error} if environment variable doesn't exist
 *
 * @return {Object} null
 * @memberOf Application.Application/Util
 */
function assertSet(arg, envName) {
  // eslint-disable-next-line no-undefined
  if (arg === '' || arg === undefined || arg === null || (typeof arg === 'number' && isNaN(arg))) {
    throw new Error(`Environment variable ${envName} must be set`)
  }
}

// Pull Hard-coded the hex AES encryption key and initialization vector from container environment variables
if (process.env.DECRYPTION_KEY && process.env.DECRYPTION_IV) {
  config.encryptionEnabled = true
  config.key = Buffer.from(process.env.DECRYPTION_KEY, 'hex')
  config.iv = Buffer.from(process.env.DECRYPTION_IV, 'hex')
}

config.port = process.env.NODE_PORT || 80

config.env = process.env.NODE_ENV || 'production'

config.features = {}

config.features.storage = process.env.ENABLE_CLOUD_STORAGE === 'true'

if (config.features.storage) {
  config.readPath = `${config.env}/files/`
  config.writePath = `${config.env}/files/`
} else {
  // Ensure the path below is writeable by the process
  config.writePath = path.join(__dirname, `../files/`)
  // Store settings and actions in the directory below, in the <filetype>/<serial>/<serial>.tzs directory structure
  config.readPath = path.join(__dirname, `../files/`)
}

if (config.features.storage) {
  assertSet(config.google.projectId, 'GOOGLE_PROJECT')
  assertSet(config.google.storage, 'GOOGLE_BUCKET')
}

// NOTE: exists but did it get parsed.
if (process.env.GOOGLE_AUTH) {
  assertSet(config.google.auth, 'GOOGLE_AUTH')
}

module.exports = config
