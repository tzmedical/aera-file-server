/* eslint-env node */
/* eslint-disable no-console */
'use strict'

// External includes
const path = require('path')
const express = require('express')

// App includes
const util = require('./util')
const config = require('./config')
const encryption = require('./encryption')

const readPath = config.readPath
const writePath = config.writePath

module.exports = (app) => {
  app.get('/', (req, res) => res.sendStatus(200))

  // Use middleware to decrypt all incoming POST data
  if (config.encryptionEnabled) {
    app.post('*', encryption.decrypt(config.key, config.iv))
  }

  // TODO consistify how actions are returned.
  app.post(
    '/:type(ecgs|events|reports)/upload/:serial/:fileName',
    express.raw({ limit: '10mb' }),
    function handleRequest(req, res, next) {
      console.log('Receiving file: ' + req.originalUrl)
      // Get file name and path from url params
      const serial = req.params.serial
      const fileName = req.params.fileName
      const fileStorageLocation = `${writePath}${req.params.type}/${serial}/${fileName}`
      // Write file to disk
      util
        .writeFile(fileStorageLocation, req.body)
        .then(async () => {
          // If the serial number starts with a non-digit, it MUST be a aera in
          // compatibility mode, so return an actions file response. Otherwise, just
          // 200 OK it.
          const regex = /^\D/
          const found = serial.match(regex)
          if (found === null) {
            // Respond with 200 OK
            res.sendStatus(200)
            return
          }

          // Send actions file instead of 200 ok
          const actionFileName = `${readPath}actions/${req.params.serial}/${req.params.serial}.tza`
          try {
            let data = await util.readFile(actionFileName)
            if (config.encryptionEnabled && req.headers['tzmedical-aes-control'] !== 'disable') {
              data = encryption.encrypt(data, config.key, config.iv)
            }
            res.send(data)
          } catch (error) {
            if (error.message.includes('No such object') && error.message.includes('tza')) {
              res.status(200).send('')
              return
            }
            throw error
          }
        })
        .catch((err) => {
          console.log(err)
          next(err)
        })
    }
  )

  app.get('/settings/download/:serial', async function handleRequest(req, res, next) {
    console.log('Serving settings: ' + req.originalUrl)
    const settingsFileName = `${readPath}settings/${req.params.serial}/${req.params.serial}.tzs`

    try {
      let data = await util.readFile(settingsFileName)
      if (config.encryptionEnabled && req.headers['tzmedical-aes-control'] !== 'disable') {
        data = encryption.encrypt(data, config.key, config.iv)
      }
      res.send(data)
    } catch (error) {
      next(error)
    }
  })

  app.get('/actions/download/:serial', async function handleRequest(req, res, next) {
    console.log('Serving actions: ' + req.originalUrl)

    const actionFileName = `${readPath}actions/${req.params.serial}/${req.params.serial}.tza`

    try {
      let data = await util.readFile(actionFileName)
      if (config.encryptionEnabled && req.headers['tzmedical-aes-control'] !== 'disable') {
        data = encryption.encrypt(data, config.key, config.iv)
      }
      res.send(data)
    } catch (error) {
      next(error)
    }
  })
}
