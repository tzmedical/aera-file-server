# Aera File Server

Serves and receives files from Aera CT devices

Decrypts incoming files with the specified encryption key and initialization vector, and decrypts files on their way out

## Setup
* Create a .env file for development and .env.production for production with key value pairs. See .env.dist for an example.
* Modify the `key` and `iv` variables for encryption settings
* Modify the `port` variable if a port other than 80 is desired
* Ensure that the `./files` directory is writable, or change the `writePath` variable to a location writable by the Node process
* Add settings and actions files to the location specified by the `readPath` variable

Note: One option for dealing with file permissions in a Docker environment is to add the line `user: root` to the `docker-compose.yml`

## Deployment

### Using Docker
Prerequisites:

* Server running the Docker daemon

Instructions:

* Modify the port in `docker-compose.yml` to change which port the application is served on from the host
* `docker-compose build`
* `docker-compose up`

### Using native node.js
Prerequisites:

* Server with node (> 4.0) installed

Instructions:

* `npm install`
* `npm start`