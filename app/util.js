/* eslint-env node */
'use strict'

// External Includes
const mkdirp = require('mkdirp')
const fs = require('fs')

// App Includes
const config = require('./config')
const storage = require('./cloud-storage')

const writeFileToFileSystem = (fileToWrite, data) => {
  return new Promise((resolve, reject) => {
    let dir = fileToWrite.split('/')
    dir.pop()
    dir = dir.join('/')

    // Make directory, recursively
    mkdirp(dir, function onDirCreated(err) {
      if (err) {
        reject(err)
      }
      // Save file to disk
      fs.writeFile(fileToWrite, data, function onWrite(e) {
        if (e) {
          reject(e)
        }
        resolve('Write Success!')
      })
    })
  })
}

const readFileFromFileSystem = (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, (err, data) => {
      if (err) {
        reject(err)
      }

      resolve(data)
    })
  })
}

const writeFileToCloudStorage = (fileName, data) => {
  const fileToWrite = fileName
  return storage.save(fileToWrite, data)
}

const readFileFromCloudStorage = (fileName) => {
  return storage.getFile(fileName)
}

const files = {}

if (config.features.storage) {
  files.readFile = readFileFromCloudStorage
  files.writeFile = writeFileToCloudStorage
} else {
  files.readFile = readFileFromFileSystem
  files.writeFile = writeFileToFileSystem
}

module.exports = files
