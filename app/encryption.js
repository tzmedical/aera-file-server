/* eslint-env node */
'use strict'

const crypto = require('crypto')

const algorithm = 'aes-128-cbc'

module.exports = {
  // TODO handle decryption failures by storing whatever was sent in
  decrypt: function decrypt(key, iv) {
    const middleware = function decryptMiddleware(req, res, next) {
      if (req.headers['tzmedical-aes-control'] !== 'disable') {
        const decipher = crypto.createDecipheriv(algorithm, key, iv)
        decipher.setAutoPadding(false)
        const decrypted = Buffer.concat([decipher.update(req.body), decipher.final()])
        req.body = decrypted
      }

      next()
    }
    return middleware
  },
  encrypt: function encrypt(data, key, iv) {
    const cipher = crypto.createCipheriv(algorithm, key, iv)
    const encrypted = Buffer.concat([cipher.update(data), cipher.final()])
    return encrypted
  },
}
