/* eslint-env node, mocha */
/*eslint max-nested-callbacks: ["error", 4]*/

const expect = require('chai').expect
const path = require('path')

const helper = require('./helper')
helper.exposeEnviromentVariables()

const config = require('../app/config')

const cloudStorage = require(path.join(__dirname, '../app/cloud-storage.js'))

const cloudStorageMethods = helper.getAllMethods(cloudStorage)

if (config.features.storage) {
  describe('Cloud Storage', () => {
    const testingFolder = `${config.env}/test`

    before(() => {
      return cloudStorage.deleteFiles(testingFolder)
    })

    describe('Create Bucket', () => {
      it('should export a function named createBucket', (done) => {
        expect(cloudStorageMethods).to.include('createBucket')
        done()
      })

      it("should create a bucket if it doesn't exist", (done) => {
        cloudStorage
          .createBucket('test')
          .then(() => done())
          .catch((err) => {
            if (
              err &&
              err.errors[0].message.includes('The requested bucket name is not available.')
            ) {
              done()
              return
            }
            done(err)
          })
      })
    })

    describe('Get file', () => {
      const buffer1 = Buffer.from('File 1', 'utf8')
      const filePath1 = `${testingFolder}/1.txt`

      it('should export a function named getFile', (done) => {
        expect(cloudStorageMethods).to.include('getFile')
        done()
      })

      it('should get a file from storage', (done) => {
        cloudStorage
          .save(filePath1, buffer1)
          .then(() => cloudStorage.getFile(`${testingFolder}/1.txt`))

          .then((file) => {
            expect(file).to.deep.equal(Buffer.from('File 1', 'utf8'))
            done()
          })
          .catch((err) => done(err))
      })
    })

    describe('Save file', () => {
      it('should export a function named save', (done) => {
        expect(cloudStorageMethods).to.include('save')
        done()
      })
    })

    describe('Delete file', () => {
      it('should export a function named deleteFiles', (done) => {
        expect(cloudStorageMethods).to.include('deleteFiles')
        done()
      })

      it('should delete all files in a given folder from cloud storage', (done) => {
        cloudStorage
          .deleteFiles(testingFolder)
          .then(() => done())
          .catch((err) => done(err))
      })
    })

    describe('Get file paths', () => {
      it('should export a function named getFilePaths', (done) => {
        expect(cloudStorageMethods).to.include('getFilePaths')
        done()
      })

      it('should get all the file paths from a specified folder', (done) => {
        const buffer1 = Buffer.from('File 1', 'utf8')
        const buffer2 = Buffer.from('File 2', 'utf8')
        const filePath1 = `${testingFolder}/1.txt`
        const filePath2 = `${testingFolder}/a/2.txt`
        const expectedFilePaths = [filePath1, filePath2]

        cloudStorage
          .save(filePath1, buffer1)
          .then(() => cloudStorage.save(filePath2, buffer2))
          .then(() => cloudStorage.getFilePaths(`${testingFolder}/`))
          .then((filePaths) => {
            expect(filePaths).to.deep.equal(expectedFilePaths)
            done()
          })
          .catch(done)
      })

      it('should get all the file paths from a specified folder with a delimiter', (done) => {
        const buffer1 = Buffer.from('File 1', 'utf8')
        const buffer2 = Buffer.from('File 2', 'utf8')
        const filePath1 = `${testingFolder}/1.txt`
        const filePath2 = `${testingFolder}/a/2.txt`
        const delimiter = '/'
        const expectedFilePaths = [filePath1]

        cloudStorage
          .save(filePath1, buffer1)
          .then(() => cloudStorage.save(filePath2, buffer2))
          .then(() => cloudStorage.getFilePaths(`${testingFolder}/`, delimiter))
          .then((filePaths) => {
            expect(filePaths).to.deep.equal(expectedFilePaths)
            done()
          })
          .catch(done)
      })
    })
  })
}
