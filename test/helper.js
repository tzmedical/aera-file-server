/* eslint-env node */
/*eslint no-loop-func: 0*/
/*eslint no-process-env: 0*/

'use strict'

const crypto = require('crypto')

const algorithm = 'aes-128-cbc'

// Bootstrap environment variables
require('dotenv').config()

function exposeEnviromentVariables() {
  if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development'
  }

  if (!process.env.NODE_PORT) {
    process.env.NODE_PORT = 3000
  }

  if (process.env.ENABLE_CLOUD_STORAGE === false && process.env.NODE_ENV === 'development') {
    process.env.ENABLE_CLOUD_STORAGE = true
  } else if (process.env.ENABLE_CLOUD_STORAGE === false) {
    process.env.ENABLE_CLOUD_STORAGE = false
  }
}

function getAllMethods(obj) {
  let props = []

  do {
    props = props.concat(Object.getOwnPropertyNames(obj))
    // eslint-disable-next-line no-param-reassign
  } while ((obj = Object.getPrototypeOf(obj)))
  return props
}

function decrypt(data, key, iv) {
  const decipher = crypto.createDecipheriv(algorithm, key, iv)
  decipher.setAutoPadding(false)
  return Buffer.concat([decipher.update(data), decipher.final()])
}

module.exports = { exposeEnviromentVariables, getAllMethods, decrypt }
